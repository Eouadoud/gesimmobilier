const PropertyOwnership = artifacts.require("PropertyOwnership");
const ethers = require("ethers");


/***
 * 
 * string memory name,
        string memory description,
        string memory propType,
        bytes32 postalAddresse,
        bytes32 pictureLink,
        uint16 surface,
        uint32 price,
        bytes32 creationDate
 */
contract("PropertyOwnership", accounts => {
    it("...should store the value 18.", async () => {
        const propertyOwnershipInstance = await PropertyOwnership.deployed();
        console.log(ethers.utils.formatBytes32String("Test test"));
        console.log(ethers.utils.formatBytes32String("87 Rue de Richelieu, 75002 Paris"));
        console.log(ethers.utils.formatBytes32String("T5 Appartement Terrasse"));
        console.log(ethers.utils.formatBytes32String("2021-01-24T03:23:11Z"));
        // Set value of 2
        await propertyOwnershipInstance.createProperty(
            ethers.utils.formatBytes32String("Test test"),
            ethers.utils.formatBytes32String("87 Rue de Richelieu, 75002 Paris"),
            ethers.utils.formatBytes32String("T5 Appartement Terrasse"),
            ethers.utils.formatBytes32String("photos Terrasse"),
            2,
            18,
            ethers.utils.formatBytes32String("2021-01-24T03:23:11Z"),

            { from: accounts[0], gas: 8000000 }
        );

        // Get stored value
        const storedData = await propertyOwnershipInstance.getProperty(1).call();
        console.log(storedData)
        assert.equal(storedData.surface, 18);
    });
});