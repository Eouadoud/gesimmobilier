pragma solidity = 0.8.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/PropertyOwnership.sol";

contract TestSimpleStorage {

  function testItStoresAValue() public {
    PropertyOwnership propertyOwnership = new PropertyOwnership();
    propertyOwnership = PropertyOwnership(DeployedAddresses.PropertyOwnership());
    // uint test = propertyOwnership;
    uint16 expectedSurface = 18;

    Assert.equal(uint(propertyOwnership.getProperty(1).surface), uint(expectedSurface), "It should store the value 89.");
  }
  
}