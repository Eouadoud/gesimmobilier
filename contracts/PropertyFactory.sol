// SPDX-License-Identifier: MIT
pragma solidity =0.8.0;

import "./librairies/Ownable.sol";
import "./librairies/SafeMath.sol";

contract PropertyFactory is Ownable {

    using SafeMath for uint256;

    modifier onlyOwnerOf(uint _propertyId) {
        require(msg.sender == propertyToOwner[_propertyId]);
        _;
    }

    struct Property {
        string name;
        string description;
        string propType;
        bytes32 postalAddresse;
        bytes32 pictureLink;
        uint16 surface;
        uint32 price;
        bytes32 creationDate;
    }

    Property[] public properties;

    mapping (uint => address) public propertyToOwner;
    mapping (address => uint[]) public ownerToProperties;
    mapping (address => uint) ownerPropertyCount;

    function createProperty(
        string memory name,
        string memory description,
        string memory propType,
        bytes32 postalAddresse,
        bytes32 pictureLink,
        uint16 surface,
        uint32 price,
        bytes32 creationDate
    ) public {
        uint id = properties.length;
        properties.push(Property(name, description, propType, postalAddresse, pictureLink, surface, price, creationDate));
        propertyToOwner[id] = msg.sender;
        ownerToProperties[msg.sender].push(id);
    }

    function getProperty(uint propertyId) public view returns (Property memory _property) {
        require(properties.length > propertyId);
        return properties[propertyId];
    }

    function getPropertiesByOwner() public view returns (uint[] memory _properties) {
        return ownerToProperties[msg.sender];
    }

    function updatePropertie(uint propertyId, string memory name, string memory description,uint32 price) public {
        require(properties.length > propertyId);
        properties[propertyId].name = name;
        properties[propertyId].description = description;
        properties[propertyId].price = price;
    }

}
