const path = require("path");
const Web3 = require("web3");
const HDWalletProvider = require("@truffle/hdwallet-provider");
require('dotenv').config();

const INFURA_API_KEY = "e97061e7be204b5c9f63ae273e5efd25";

// const FS = require('fs');
const MNEMONIC = "poet stand session notable poverty flag clump legal surface echo defy because"

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "frontend/src/contracts"),
  networks: {
    develop: {
      port: 9545,
      network_id: '*',
      accounts: 5,
      defaultEtherBalance: 600,
      blockTime: 3,
      gasPrice: 12,
      gas: 4700000
    },
    kovan: {
      provider: function() {
        return new HDWalletProvider(MNEMONIC, `https://kovan.infura.io/v3/${INFURA_API_KEY}`)
      },
      network_id: 42
    }
  },
  compilers: {
    solc: {
      version: "0.8.0",
    },
  }
};
