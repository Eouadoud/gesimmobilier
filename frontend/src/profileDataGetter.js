import w3getter from "./w3getter";
import PropertyOwnershipContract from "./contracts/PropertyOwnership.json";

const loadComponentData = async () => {
    console.log("loading")
    try {
        const web3 = await w3getter();
        const accounts = await web3.eth.getAccounts();
        const networkId = await web3.eth.net.getId();
        const deployedNetwork = PropertyOwnershipContract.networks[networkId];
        const instance = new web3.eth.Contract(
            PropertyOwnershipContract.abi,
            deployedNetwork && deployedNetwork.address,
        );
        return {web3, accounts, contract: instance};
    } catch (error) {
        // Catch any errors for any of the above operations.
        swal(
            "erreur","erreur de chargement de données profile", "error"
        );
        console.error(error);
    }
};

export default {
    loadComponentData,
}