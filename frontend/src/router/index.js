import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import PropForm from '@/components/Property-form'
import PropsList from '@/components/Properties-list'
import offersList from '@/components/Offers-list.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/newprop',
      name:'PropForm',
      component: PropForm
    },
    {
      path:'/myprops',
      name:'PropsList',
      component: PropsList
    },
    {
      path:'/offers',
      name:'OffersList',
      component: offersList
    }

  ]
})
